# Pirate-box

Docker container running bittorrent and vpn

1. Make branch from master, pull files.
2. Change vpn to "no" in dc file
3. dcup - This will create the container, as well as the persistent volume, and allow for everything to be built.
4. Edit credentials.conf to replace placeholder with real creds.
5. 
```
docker cp pia.ovpn qbittorrent:/config/openvpn/
docker cp credentials.conf qbittorrent:/config/openvpn/
```
6. dcdown
7. change vpn to "yes" in dc file
8. dcup
